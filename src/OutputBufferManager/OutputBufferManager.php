<?php

declare(strict_types=1);

namespace Interitty\OutputBufferManager;

use Interitty\Exceptions\Exceptions;
use Interitty\OutputBufferManager\Handler\Handler;
use Interitty\OutputBufferManager\Handler\HandlerFactory;
use Interitty\OutputBufferManager\Handler\HandlerFactoryInterface;
use Interitty\OutputBufferManager\Handler\HandlerInterface;
use Interitty\Utils\SmartObject;
use Interitty\Utils\Validators;
use LogicException;

use function array_key_exists;
use function array_keys;
use function assert;
use function call_user_func_array;
use function end;
use function in_array;
use function ob_end_flush;
use function ob_flush;
use function ob_get_status;
use function ob_start;

class OutputBufferManager
{
    use SmartObject;

    /** @var int */
    protected int $chunkSize = 0;

    /** @var string|null */
    protected ?string $currentHandlerName = null;

    /** @var HandlerFactoryInterface */
    protected HandlerFactoryInterface $handlerFactory;

    /** @var HandlerInterface[] */
    protected array $handlers = [];

    /**
     * Begin output buffering
     *
     * @param string $handlerName
     * @phpstan-param callable(string, int): string $handler
     * @return Handler
     */
    public function begin(string $handlerName, callable $handler): Handler
    {
        $handlerService = $this->getHandlerFactory()->create($handlerName, $handler);
        $this->beginHandlerService($handlerService);
        return $handlerService;
    }

    /**
     * Begin output buffering by Handler service
     *
     * @param HandlerInterface $handlerService
     * @return void
     */
    public function beginHandlerService(HandlerInterface $handlerService): void
    {
        $handlerName = $handlerService->getHandlerName();
        if (array_key_exists($handlerName, $this->handlers) !== false) {
            throw Exceptions::extend(LogicException::class)
                    ->setMessage('Output buffer handler ":handler" is already registered')
                    ->addData('handler', $handlerName);
        }
        $this->handlers[$handlerName] = $handlerService;
        $this->currentHandlerName = null;
        $chunkSize = $this->getChunkSize();
        ob_start([$this, 'processManageOutput'], $chunkSize);
        $handlerService->processBegin();
    }

    /**
     * End output buffering
     *
     * @param string $handlerName
     * @return void
     */
    public function end(string $handlerName): void
    {
        $this->processValidateExpectedBuffer($handlerName);
        ob_end_flush();
        $handlerService = $this->handlers[$handlerName];
        $handlerService->processEnd();
        unset($this->handlers[$handlerName]);
        $this->currentHandlerName = null;
    }

    /**
     * Flush output buffer
     *
     * @param string $handlerName
     * @return void
     */
    public function flush(string $handlerName): void
    {
        $this->processValidateExpectedBuffer($handlerName);
        ob_flush();
    }

    // <editor-fold defaultstate="collapsed" desc="Processors">
    /**
     * Detect current handler name processor
     *
     * @return string|null
     */
    protected function processDetectCurrentHandlerName(): ?string
    {
        $handlerNames = $this->getHandlerNames();
        $currentHandlerName = end($handlerNames);
        if ($currentHandlerName === false) {
            $currentHandlerName = null;
        }
        return $currentHandlerName;
    }

    /**
     * Manage output processor
     *
     * @param string $output
     * @param int $phase Bitmask of PHP_OUTPUT_HANDLER_* constants.
     * @return string
     * @internal Used as callback in OutputBufferManager::begin
     */
    public function processManageOutput(string $output, int $phase): string
    {
        $handlerName = $this->getCurrentHandlerName();
        $managedOutput = '';
        if (($handlerName !== null) && (array_key_exists($handlerName, $this->handlers) === true)) {
            $handlerService = $this->handlers[$handlerName];
            /** @phpstan-var string $managedOutput */
            $managedOutput = call_user_func_array([$handlerService, 'processManageOutput'], [$output, $phase]);
        }
        return $managedOutput;
    }

    /**
     * Validate expected buffer processor
     *
     * @param string $handlerName
     * @return void
     */
    protected function processValidateExpectedBuffer(string $handlerName): void
    {
        if ($this->isExpectedBuffer($handlerName) !== true) {
            $handlerNames = $this->getHandlerNames();
            $currentHandlerName = $this->getCurrentHandlerName();
            $currentBufferManaged = $this->isCurrentBufferManaged();
            if ($currentBufferManaged !== true) {
                throw Exceptions::extend(LogicException::class)
                        ->setMessage('Current output buffer manager is not OutputBufferManager');
            } elseif (in_array($handlerName, $handlerNames, true) !== true) {
                throw Exceptions::extend(LogicException::class)
                        ->setMessage('Expected output buffer handler ":handler" is not registered')
                        ->addData('handler', $handlerName);
            } elseif ($currentHandlerName !== $handlerName) {
                throw Exceptions::extend(LogicException::class)
                        ->setMessage('Current output buffer handler ":currentHandler" is not expected ":handler"')
                        ->addData('currentHandler', $currentHandlerName)
                        ->addData('handler', $handlerName);
            }
        }
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * ChunkSize getter
     *
     * @return int
     */
    public function getChunkSize(): int
    {
        return $this->chunkSize;
    }

    /**
     * ChunkSize setter
     *
     * @param int $chunkSize
     * @return static Provides fluent interface
     */
    public function setChunkSize(int $chunkSize): static
    {
        $this->chunkSize = $chunkSize;
        return $this;
    }

    /**
     * Singleton implementation of current handler name getter
     *
     * @return string|null
     */
    public function getCurrentHandlerName(): ?string
    {
        if ($this->currentHandlerName === null) {
            $currentHandlerName = $this->processDetectCurrentHandlerName();
            $this->setCurrentHandlerName($currentHandlerName);
        }
        return $this->currentHandlerName;
    }

    /**
     * Current handler name setter
     *
     * @param string|null $currentHandlerName
     * @return static Provides fluent interface
     */
    protected function setCurrentHandlerName(?string $currentHandlerName): static
    {
        $this->currentHandlerName = $currentHandlerName;
        return $this;
    }

    /**
     * Current buffer managed checker
     *
     * @return bool
     */
    public function isCurrentBufferManaged(): bool
    {
        $currentBufferStatus = ob_get_status();
        $outputBufferManager = array_key_exists('name', $currentBufferStatus) ? $currentBufferStatus['name'] : null;
        $currentBufferManaged = ($outputBufferManager === __CLASS__ . '::processManageOutput');
        return $currentBufferManaged;
    }

    /**
     * Expected buffer checker
     *
     * @param string $handlerName
     * @return bool
     */
    public function isExpectedBuffer(string $handlerName): bool
    {
        $isExpected = (($this->isCurrentBufferManaged() === true) && ($this->getCurrentHandlerName() === $handlerName));
        return $isExpected;
    }

    /**
     * HandlerFactory getter
     *
     * @return HandlerFactoryInterface
     */
    public function getHandlerFactory(): HandlerFactoryInterface
    {
        if (isset($this->handlerFactory) === false) {
            $handlerFactory = new HandlerFactory();
            $this->setHandlerFactory($handlerFactory);
        }
        return $this->handlerFactory;
    }

    /**
     * HandlerFactory setter
     *
     * @param HandlerFactoryInterface $handlerFactory
     * @return static Provides fluent interface
     */
    public function setHandlerFactory(HandlerFactoryInterface $handlerFactory): static
    {
        assert(Validators::check(isset($this->handlerFactory), 'uninitialized', 'handlerFactory before set'));
        $this->handlerFactory = $handlerFactory;
        return $this;
    }

    /**
     * Handler names getter
     *
     * @return string[]
     */
    public function getHandlerNames(): array
    {
        $handlerNames = array_keys($this->handlers);
        return $handlerNames;
    }

    // </editor-fold>
}
