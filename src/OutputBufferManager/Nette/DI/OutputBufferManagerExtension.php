<?php

declare(strict_types=1);

namespace Interitty\OutputBufferManager\Nette\DI;

use Interitty\DI\CompilerExtension;
use Interitty\OutputBufferManager\OutputBufferManager;

class OutputBufferManagerExtension extends CompilerExtension
{
    /** All available provider name constants */
    public const string PROVIDER_OUTPUT_BUFFER_MANAGER = 'outputBufferManager';

    /**
     * @inheritdoc
     */
    public function beforeCompile(): void
    {
        $this->setupOutputBufferManager();
    }

    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * Output buffer manager setup helper
     *
     * @return void
     */
    protected function setupOutputBufferManager(): void
    {
        if ($this->setupServiceAlias(OutputBufferManager::class, self::PROVIDER_OUTPUT_BUFFER_MANAGER) === false) {
            $builder = $this->getContainerBuilder();
            $builder->addDefinition($this->prefix(self::PROVIDER_OUTPUT_BUFFER_MANAGER))
                ->setFactory(OutputBufferManager::class);
        }
    }
    // </editor-fold>
}
