<?php

declare(strict_types=1);

namespace Interitty\OutputBufferManager\Handler;

interface HandlerFactoryInterface
{
    /**
     * Handler factory
     *
     * @param string $handlerName
     * @phpstan-param callable(string, int): string $handler
     * @return Handler
     */
    public function create(string $handlerName, callable $handler): Handler;
}
