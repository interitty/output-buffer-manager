<?php

declare(strict_types=1);

namespace Interitty\OutputBufferManager\Handler;

class HandlerFactory implements HandlerFactoryInterface
{
    /**
     * @inheritdoc
     */
    public function create(string $handlerName, callable $handler): Handler
    {
        $handlerService = new Handler($handlerName, $handler);
        return $handlerService;
    }
}
