<?php

declare(strict_types=1);

namespace Interitty\OutputBufferManager\Handler;

use function call_user_func_array;

class Handler implements HandlerInterface
{
    /** @phpstan-var callable(string, int): string */
    protected $handler;

    /** @var string */
    protected string $handlerName;

    /** @var bool */
    protected bool $inProgress = false;

    /**
     * Constructor
     *
     * @param string $handlerName
     * @phpstan-param callable(string, int): string $handler
     * @return void
     */
    public function __construct(string $handlerName, callable $handler)
    {
        $this->setHandlerName($handlerName);
        $this->setHandler($handler);
    }

    /**
     * @inheritdoc
     */
    public function processBegin(): void
    {
        $this->setInProgress(true);
    }

    /**
     * @inheritdoc
     */
    public function processEnd(): void
    {
        $this->setInProgress(false);
    }

    /**
     * @inheritdoc
     */
    public function processManageOutput(string $bufferOutput): string
    {
        $handler = $this->getHandler();
        /** @var string $response */
        $response = call_user_func_array($handler, [$bufferOutput]);
        return $response;
    }
    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">

    /**
     * Handler getter
     *
     * @return callable(string, int): string
     */
    protected function getHandler(): callable
    {
        return $this->handler;
    }

    /**
     * Handler setter
     *
     * @param callable(string, int): string $handler
     * @return static Provides fluent interface
     */
    protected function setHandler(callable $handler): static
    {
        $this->handler = $handler;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getHandlerName(): string
    {
        return $this->handlerName;
    }

    /**
     * HandlerName setter
     *
     * @param string $handlerName
     * @return static Provides fluent interface
     */
    protected function setHandlerName(string $handlerName): static
    {
        $this->handlerName = $handlerName;
        return $this;
    }

    /**
     * InProgress checker
     *
     * @return bool
     */
    public function isInProgress(): bool
    {
        return $this->inProgress;
    }

    /**
     * InProgress setter
     *
     * @param bool $inProgress
     * @return static Provides fluent interface
     */
    protected function setInProgress(bool $inProgress): static
    {
        $this->inProgress = $inProgress;
        return $this;
    }
    // </editor-fold>
}
