<?php

declare(strict_types=1);

namespace Interitty\OutputBufferManager\Handler;

interface HandlerInterface
{
    /**
     * Begin event processor
     *
     * @return void
     */
    public function processBegin(): void;

    /**
     * End event processor
     *
     * @return void
     */
    public function processEnd(): void;

    /**
     * ManageOutput event processor
     *
     * @param string $bufferOutput
     * @return string
     */
    public function processManageOutput(string $bufferOutput): string;

    /**
     * Handler name getter
     *
     * @return string
     */
    public function getHandlerName(): string;
}
