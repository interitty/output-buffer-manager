<?php

declare(strict_types=1);

namespace Interitty\OutputBufferManager\Handler;

use Interitty\OutputBufferManager\OutputBufferHandlerTrait;
use Interitty\PhpUnit\BaseTestCase;

/**
 * @coversDefaultClass Interitty\OutputBufferManager\Handler\HandlerFactory
 */
class HandlerFactoryTest extends BaseTestCase
{
    use OutputBufferHandlerTrait;

    /**
     * Create method test
     *
     * @return void
     * @covers ::create
     */
    public function testCreate(): void
    {
        $handlerName = 'handlerName';
        $handler = $this->createEmptyHandler();
        $handlerFactory = new HandlerFactory();
        $handlerService = $handlerFactory->create($handlerName, $handler);
        self::assertSame($handlerName, $handlerService->getHandlerName());
    }
}
