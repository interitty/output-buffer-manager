<?php

declare(strict_types=1);

namespace Interitty\OutputBufferManager\Handler;

use Interitty\OutputBufferManager\OutputBufferHandlerTrait;
use Interitty\PhpUnit\BaseTestCase;

/**
 * @coversDefaultClass Interitty\OutputBufferManager\Handler\Handler
 */
class HandlerTest extends BaseTestCase
{
    use OutputBufferHandlerTrait;

    /**
     * Handler basics test
     *
     * @return void
     * @covers ::__construct
     * @covers ::getHandler
     * @covers ::setHandler
     * @covers ::getHandlerName
     * @covers ::setHandlerName
     * @group integration
     */
    public function testHandler(): void
    {
        $handler = $this->createEmptyHandler();
        $handlerName = 'handlerName';
        $handlerService = new Handler($handlerName, $handler);
        self::assertSame($handler, $this->callNonPublicMethod($handlerService, 'getHandler'));
        self::assertSame($handlerName, $handlerService->getHandlerName());
    }

    /**
     * Handler begin end test
     *
     * @return void
     * @covers ::processBegin
     * @covers ::processEnd
     * @covers ::isInProgress
     * @covers ::setInProgress
     * @group integration
     */
    public function testHandlerBeginEnd(): void
    {
        $handler = $this->createEmptyHandler();
        $handlerName = 'handlerName';
        $handlerService = new Handler($handlerName, $handler);
        self::assertFalse($handlerService->isInProgress());
        $handlerService->processBegin();
        self::assertTrue($handlerService->isInProgress());
        $handlerService->processEnd();
        self::assertFalse($handlerService->isInProgress());
    }

    /**
     * Manage output processor test
     *
     * @return void
     * @covers ::processManageOutput
     * @group integration
     */
    public function testProcessManageOutput(): void
    {
        $output = null;
        $handler = $this->createProxyHandler($output);
        $handlerName = 'handlerName';
        $testContent = 'testContent';
        $handlerService = new Handler($handlerName, $handler);

        self::assertSame($testContent, $handlerService->processManageOutput($testContent));
        self::assertSame($testContent, $output);
    }
}
