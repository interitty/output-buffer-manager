<?php

declare(strict_types=1);

namespace Interitty\OutputBufferManager;

use Generator;
use Interitty\OutputBufferManager\Handler\HandlerFactory;
use Interitty\OutputBufferManager\Handler\HandlerFactoryInterface;
use Interitty\PhpUnit\BaseTestCase;
use LogicException;
use PHPUnit\Framework\MockObject\MockObject;

use function ob_get_clean;
use function ob_start;

use const PHP_EOL;

/**
 * @coversDefaultClass Interitty\OutputBufferManager\OutputBufferManager
 */
class OutputBufferManagerTest extends BaseTestCase
{
    use OutputBufferHandlerTrait;

    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Buffer already registered test
     *
     * @return void
     * @covers ::begin
     * @covers ::beginHandlerService
     * @covers ::processValidateExpectedBuffer
     * @covers ::isExpectedBuffer
     * @group integration
     * @group negative
     */
    public function testBufferAlreadyRegistered(): void
    {
        $handlerName = 'handlerName';
        $emptyHandler = $this->createEmptyHandler();
        $outputBufferManager = new OutputBufferManager();
        $outputBufferManager->begin($handlerName, $emptyHandler);
        $this->expectException(LogicException::class);
        $this->expectExceptionMessage('Output buffer handler ":handler" is already registered');
        $this->expectExceptionData(['handler' => $handlerName]);
        try {
            $outputBufferManager->begin($handlerName, $emptyHandler);
        } catch (LogicException $exception) {
            $outputBufferManager->end($handlerName);
            throw $exception;
        }
    }

    /**
     * Buffer not expected test
     *
     * @return void
     * @covers ::end
     * @covers ::processValidateExpectedBuffer
     * @covers ::isExpectedBuffer
     * @group integration
     * @group negative
     */
    public function testBufferNotExpected(): void
    {
        $handlerName = 'handlerName';
        $secondHandlerName = 'secondHandlerName';
        $emptyHandler = $this->createEmptyHandler();
        $outputBufferManager = new OutputBufferManager();
        $outputBufferManager->begin($handlerName, $emptyHandler);
        $outputBufferManager->begin($secondHandlerName, $emptyHandler);
        $this->expectException(LogicException::class);
        $this->expectExceptionMessage('Current output buffer handler ":currentHandler" is not expected ":handler"');
        $this->expectExceptionData(['currentHandler' => $secondHandlerName, 'handler' => $handlerName]);
        try {
            $outputBufferManager->end($handlerName);
        } catch (LogicException $exception) {
            $outputBufferManager->end($secondHandlerName);
            $outputBufferManager->end($handlerName);
            throw $exception;
        }
    }

    /**
     * Buffer not managed test
     *
     * @return void
     * @covers ::end
     * @covers ::processValidateExpectedBuffer
     * @covers ::isExpectedBuffer
     * @group integration
     * @group negative
     */
    public function testBufferNotManaged(): void
    {
        $handlerName = 'handlerName';
        $outputBufferManager = new OutputBufferManager();
        $this->expectException(LogicException::class);
        $this->expectExceptionMessage('Current output buffer manager is not OutputBufferManager');
        $outputBufferManager->end($handlerName);
    }

    /**
     * Buffer not registered test
     *
     * @return void
     * @covers ::end
     * @covers ::processValidateExpectedBuffer
     * @covers ::isExpectedBuffer
     * @group integration
     * @group negative
     */
    public function testBufferNotRegistered(): void
    {
        $handlerName = 'handlerName';
        $notRegisteredHandler = 'notRegisteredHandlerName';
        $emptyHandler = $this->createEmptyHandler();
        $outputBufferManager = new OutputBufferManager();
        $outputBufferManager->begin($handlerName, $emptyHandler);
        $this->expectException(LogicException::class);
        $this->expectExceptionMessage('Expected output buffer handler ":handler" is not registered');
        $this->expectExceptionData(['handler' => $notRegisteredHandler]);
        try {
            $outputBufferManager->end($notRegisteredHandler);
        } catch (LogicException $exception) {
            $outputBufferManager->end($handlerName);
            throw $exception;
        }
    }

    /**
     * Catch content test
     *
     * @return void
     * @covers ::begin
     * @covers ::beginHandlerService
     * @covers ::end
     * @group integration
     */
    public function testCatchContent(): void
    {
        ob_start();
        $testContent = 'testContent';
        $handlerName = 'handlerName';
        $output = null;
        $handler = $this->createCatchHandler($output);
        $outputBufferManager = new OutputBufferManager();
        $outputBufferManager->begin($handlerName, $handler);
        echo $testContent;
        $outputBufferManager->end($handlerName);
        self::assertSame($testContent, $output);
        self::assertEmpty(ob_get_clean());
    }

    /**
     * Tester of CurrentBufferManaged checker
     *
     * @return void
     * @covers ::isCurrentBufferManaged
     * @group integration
     */
    public function testCurrentBufferManaged(): void
    {
        $handler = $this->createEmptyHandler();
        $handlerName = 'handlerName';
        $outputBufferManager = new OutputBufferManager();

        self::assertFalse($outputBufferManager->isCurrentBufferManaged());
        $outputBufferManager->begin($handlerName, $handler);
        self::assertTrue($outputBufferManager->isCurrentBufferManaged());
        $outputBufferManager->end($handlerName);
        self::assertFalse($outputBufferManager->isCurrentBufferManaged());
    }

    /**
     * Handler name manipulation test
     *
     * @return void
     * @covers ::processDetectCurrentHandlerName
     * @covers ::getCurrentHandlerName
     * @covers ::getHandlerNames
     * @group integration
     */
    public function testCurrentHandlerName(): void
    {
        $handler = $this->createEmptyHandler();
        $handlerName = 'handlerName';
        $secondHandlerName = 'secondHandlerName';
        $outputBufferManager = new OutputBufferManager();
        self::assertNull($outputBufferManager->getCurrentHandlerName());
        self::assertEmpty($outputBufferManager->getHandlerNames());

        $outputBufferManager->begin($handlerName, $handler);
        self::assertSame($handlerName, $outputBufferManager->getCurrentHandlerName());
        self::assertCount(1, $outputBufferManager->getHandlerNames());

        $outputBufferManager->begin($secondHandlerName, $handler);
        self::assertSame($secondHandlerName, $outputBufferManager->getCurrentHandlerName());
        self::assertCount(2, $outputBufferManager->getHandlerNames());

        $outputBufferManager->end($secondHandlerName);
        /**
         * @phpstan-var string $handlerName
         * @phpstan-ignore varTag.nativeType
         */
        self::assertSame($handlerName, $outputBufferManager->getCurrentHandlerName());
        self::assertCount(1, $outputBufferManager->getHandlerNames());

        $outputBufferManager->end($handlerName);
        self::assertNull($outputBufferManager->getCurrentHandlerName());
        self::assertEmpty($outputBufferManager->getHandlerNames());
    }

    /**
     * Flush data provider
     *
     * @phpstan-return Generator<string, array{0: int|float|string|bool|null, 1: mixed}>
     */
    public function flushDataProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="string">
        yield 'string' => ['testContent', 'testContent'];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="null">
        yield 'null' => [null, ''];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="integer">
        yield 'integer' => [12345, '12345'];
        // </editor-fold>
    }

    /**
     * Flush test
     *
     * @param int|float|string|bool|null $testContent
     * @param mixed $expectedOutput
     * @return void
     * @covers ::flush
     * @covers ::processManageOutput
     * @dataProvider flushDataProvider
     * @group integration
     */
    public function testFlush(int|float|string|bool|null $testContent, mixed $expectedOutput): void
    {
        ob_start();
        $handlerName = 'handlerName';
        $output = null;
        $handler = $this->createCatchHandler($output);
        $outputBufferManager = new OutputBufferManager();
        $outputBufferManager->begin($handlerName, $handler);
        echo $testContent;
        self::assertNull($output);
        $outputBufferManager->flush($handlerName);
        self::assertSame($expectedOutput, $output);
        $outputBufferManager->end($handlerName);
        self::assertEmpty(ob_get_clean());
    }

    /**
     * Tester of CurrentHandlerName getter/setter implementation
     *
     * @return void
     * @covers ::getCurrentHandlerName
     * @covers ::setCurrentHandlerName
     */
    public function testGetSetCurrentHandlerName(): void
    {
        $currentHandlerName = 'currentHandlerName';
        $this->processTestGetSet(OutputBufferManager::class, 'currentHandlerName', $currentHandlerName);
    }

    /**
     * Tester of HandlerFactory getter/setter implementation
     *
     * @return void
     * @covers ::getHandlerFactory
     * @covers ::setHandlerFactory
     */
    public function testGetSetHandlerFactory(): void
    {
        $handlerFactoryMock = $this->createHandlerFactoryMock();
        $outputBufferManager = new OutputBufferManager();
        self::assertSame($outputBufferManager, $outputBufferManager->setHandlerFactory($handlerFactoryMock));
        self::assertSame($handlerFactoryMock, $outputBufferManager->getHandlerFactory());
    }

    /**
     * Tester of ChunkSize getter/setter implementation
     *
     * @return void
     * @covers ::getChunkSize
     * @covers ::setChunkSize
     * @group integration
     */
    public function testGetSetChunkSize(): void
    {
        $defaultChunkSize = 0;
        $chunkSize = 1024;
        $outputBufferManager = new OutputBufferManager();
        self::assertSame($defaultChunkSize, $outputBufferManager->getChunkSize());
        self::assertSame($outputBufferManager, $outputBufferManager->setChunkSize($chunkSize));
        self::assertSame($chunkSize, $outputBufferManager->getChunkSize());
    }

    /**
     * Tester of HandlerFactory default getter
     *
     * @return void
     * @covers ::getHandlerFactory
     * @covers ::setHandlerFactory
     */
    public function testGetDefaultHandlerFactory(): void
    {
        $outputBufferManager = new OutputBufferManager();
        self::assertInstanceOf(HandlerFactory::class, $outputBufferManager->getHandlerFactory());
    }

    /**
     * Manage content test
     *
     * @return void
     * @covers ::begin
     * @covers ::end
     * @group integration
     */
    public function testManageContentProxy(): void
    {
        ob_start();
        $testContent = 'testContent';
        $handlerName = 'handlerName';
        $output = null;
        $handler = $this->createProxyHandler($output);
        $outputBufferManager = new OutputBufferManager();
        $outputBufferManager->begin($handlerName, $handler);
        echo $testContent;
        $outputBufferManager->end($handlerName);
        self::assertSame($testContent, $output);
        self::assertSame($testContent, ob_get_clean());
    }

    /**
     * Multiline content test
     *
     * @return void
     * @covers ::begin
     * @covers ::end
     * @group integration
     */
    public function testMultilineContent(): void
    {
        ob_start();
        $testContent = 'testContent' . PHP_EOL;
        $handlerName = 'handlerName';
        $output = null;
        $handler = $this->createCatchHandler($output);
        $outputBufferManager = new OutputBufferManager();
        $outputBufferManager->begin($handlerName, $handler);
        echo $testContent;
        echo $testContent;
        echo $testContent;
        $outputBufferManager->end($handlerName);
        self::assertSame($testContent . $testContent . $testContent, $output);
        self::assertEmpty(ob_get_clean());
    }

    /**
     * Multiple buffer test
     *
     * @return void
     * @covers ::begin
     * @covers ::end
     * @group integration
     */
    public function testMultipleBuffer(): void
    {
        ob_start();
        $testContent = 'testContent';
        $secondTestContent = 'secondTestContent';
        $handlerName = 'handlerName';
        $secondHandlerName = 'secondHandlerName';
        $output = null;
        $secondOutput = null;
        $handler = $this->createCatchHandler($output);
        $secondHandler = $this->createCatchHandler($secondOutput);
        $outputBufferManager = new OutputBufferManager();
        $outputBufferManager->begin($handlerName, $handler);
        $outputBufferManager->begin($secondHandlerName, $secondHandler);
        echo $secondTestContent;
        $outputBufferManager->end($secondHandlerName);
        echo $testContent;
        $outputBufferManager->end($handlerName);
        self::assertSame($testContent, $output);
        self::assertSame($secondTestContent, $secondOutput);
        self::assertEmpty(ob_get_clean());
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">

    /**
     * HandlerFactory mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return HandlerFactoryInterface|MockObject
     */
    protected function createHandlerFactoryMock(array $methods = [])
    {
        $mock = $this->getMockForAbstractClass(HandlerFactoryInterface::class, $methods);
        return $mock;
    }
    // </editor-fold>
}
