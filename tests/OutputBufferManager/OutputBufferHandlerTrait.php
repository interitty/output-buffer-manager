<?php

declare(strict_types=1);

namespace Interitty\OutputBufferManager;

trait OutputBufferHandlerTrait
{
    // <editor-fold defaultstate="collapsed" desc="Helpers">

    /**
     * Catch handler factory
     *
     * @param string|null $output
     * @return callable
     */
    protected function createCatchHandler(?string &$output): callable
    {
        return static function (string $bufferOutput) use (&$output): string {
            $output .= $bufferOutput;
            return '';
        };
    }

    /**
     * Empty handler factory
     *
     * @return callable
     */
    protected function createEmptyHandler(): callable
    {
        return static function (): string {
            return '';
        };
    }

    /**
     * Proxy handler factory
     *
     * @param string|null $output
     * @return callable
     */
    protected function createProxyHandler(?string &$output): callable
    {
        return static function (string $bufferOutput) use (&$output): string {
            $output .= $bufferOutput;
            return $bufferOutput;
        };
    }
    // </editor-fold>
}
