<?php

declare(strict_types=1);

namespace Interitty\OutputBufferManager\Nette\DI;

use Interitty\OutputBufferManager\OutputBufferManager;
use Interitty\PhpUnit\BaseIntegrationTestCase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @coversDefaultClass Interitty\OutputBufferManager\Nette\DI\OutputBufferManagerExtension
 */
class OutputBufferManagerExtensionTest extends BaseIntegrationTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">

    /**
     * Tester of OutputBufferManager setup
     *
     * @return void
     * @covers ::setupOutputBufferManager
     * @group integration
     */
    public function testSetupOutputBufferManager(): void
    {
        $configContent = '
extensions:
    outputBufferManager: Interitty\OutputBufferManager\Nette\DI\OutputBufferManagerExtension
';
        $configFile = $this->createTempFile($configContent, 'config.neon');
        $container = $this->createContainer($configFile);

        $serviceName = 'outputBufferManager.' . OutputBufferManagerExtension::PROVIDER_OUTPUT_BUFFER_MANAGER;
        self::assertTrue($container->hasService($serviceName));
        self::assertInstanceOf(OutputBufferManager::class, $container->getService($serviceName));
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Unit tests">

    /**
     * Tester of beforeCompile
     *
     * @return void
     * @covers ::beforeCompile
     */
    public function testBeforeCompile(): void
    {
        $extension = $this->createOutputBufferManagerExtensionMock([
            'setupOutputBufferManager',
        ]);
        $extension->expects(self::once())->method('setupOutputBufferManager');
        $extension->beforeCompile();
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">

    /**
     * OutputBufferManagerExtension mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return OutputBufferManagerExtension|MockObject
     */
    protected function createOutputBufferManagerExtensionMock(array $methods = []): MockObject
    {
        $mock = $this->createPartialMock(OutputBufferManagerExtension::class, $methods);
        return $mock;
    }
    // </editor-fold>
}
